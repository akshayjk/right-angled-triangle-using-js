console.clear(); // to clear the console

let i = 6; // height of the right triangle

// This For loop is used to create a right triangle by repeat method
// only 'i' no. of console.log() calls are made.

for (let j = 1; j <= i; j++) {
    console.log('+'.repeat(j)); //change the font to make triangle of another type
}